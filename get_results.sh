#!/bin/bash

for i in 01; # 02 03 04;
do
  case="GdCl3_${i}"
  echo "-- $(date): Case  ${case}"
  pushd "${case}"
  
  # Extract energies
  echo "  Extracting energies into ene_conv.dat"
  grep :ENE "${case}.scf" | awk 'BEGIN {step=1} NR==1 {ene0=$NF; print step,ene0-$NF ; step=step+1} NR>1 {ene=$NF-ene0; print step,ene; step=step+1}' > ene_conv.dat
  echo "  Extracting dis into dis_conv.dat"
  grep :DIS "${case}.scf" | awk 'BEGIN {step=1} {ene=$NF; print step,ene; step=step+1}' > dis_conv.dat
  echo "  Extracting total magnetic moments into mmt_conv.dat"
  grep :MMT "${case}.scf" | awk 'BEGIN {step=1} {ene=$NF; print step,ene; step=step+1}' > mmt_conv.dat
  
  finalgap=$(grepline ':GAP' "${case}.scf" 1 | tail -n +2 | awk '{print $7}')
  printf "  FINAL GAP: %7.3f (eV)\n" ${finalgap}
  finalene=$(grepline ':ENE' "${case}.scf" 1 | tail -n +2 | awk '{print $NF}')
  printf "  FINAL SCF ENERGY: %20.12f (eV)\n\n" ${finalene}
  
  printf "  FINAL MAGNETIC MOMENT ON ATOMS:\n"
  grepline ':MMI00' "${case}.scf" 2 | tail -n +2
  popd
done
