#!/bin/bash

for i in 01; #02 03 04;
do
  case="GdCl3_${i}"
  echo "-- $(date): Running case ${i}"
  mkdir "${case}" && pushd "${case}" > /dev/null
  echo "  Downloading cif file"
  wget http://wien2k.at/Depository/GdCl3.cif 
  mv GdCl3.cif "${case}.cif"
  echo "  Running initialization"
  cif2struct "${case}.cif" > cif2struct.stdout
  echo "  Setting RMT"
  setrmt > setrmt.stdout
  cp "${case}.struct_setrmt" "${case}.struct"
  echo "  Running initialization"
  init_lapw -sp -numk 700 -rkmax 7 > init_lapw.stdout
  echo "  Running SCF"
  runsp_lapw > runsp.stdout 2> runsp.stderr
  echo "-- $(date) DONE"
  popd > /dev/null
done

